'use strict';
document.addEventListener('DOMContentLoaded', () => {

    {
        const keyboardButton = document.querySelector('.search-form__keyboard'),
            keyboard = document.querySelector('.keyboard'),
            CloseKeyboard = document.getElementById('close-keyboard'),
            InputSearch = document.querySelector('.search-form__input');

        const toggleKeyBoard = () => keyboard.style.top = keyboard.style.top ? '' : '50%';

        const typing = (event) => {
            const target = event.target;


            if ( target.tagName === 'BUTTON' ) {
                InputSearch.value += target.textContent.trim();
            }
        };

        keyboardButton.addEventListener('click', toggleKeyBoard );
        CloseKeyboard.addEventListener('click', toggleKeyBoard );
        keyboard.addEventListener('click', typing );

        {

            const sidebarMenu = document.querySelector('.sidebarMenu'),
                menuIcon = document.querySelector('.spinner');

            menuIcon.addEventListener( 'click', () => {
                menuIcon.classList.toggle('active');
                sidebarMenu.classList.toggle('rollUp');
            });

            sidebarMenu.addEventListener( 'click', e => {
                let target = e.target;
                    target = target.closest('a[href="#"]');

                if (target) {
                    const parentTarget = target.parentElement;

                    sidebarMenu.querySelectorAll('li').forEach( elem => {


                        if ( elem === parentTarget ) {
                            elem.classList.add('active');
                        } else {
                            elem.classList.remove('active')
                        }
                    })
                }

                console.log(parentTarget);
            });

        }

    }
});